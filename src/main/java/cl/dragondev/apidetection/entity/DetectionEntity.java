package cl.dragondev.apidetection.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "detection_data", schema = "public")
public class DetectionEntity implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "label")
    private String label;

    @Column(name = "orig_detection_date")
    private Timestamp detectionDate;

    @Column(name = "confidence")
    private float confidenceLevel;

    @Column(name = "topleft_x")
    private float topLeftX;

    @Column(name = "topleft_y")
    private float topLeftY;

    @Column(name = "bottomright_x")
    private float bottomRightX;

    @Column(name = "bottomright_y")
    private float bottomRightY;

    @Column(name = "detection_source")
    private String detectionSource;

    @Column(name = "created_app_id")
    private String createAppId;

    @Column(name = "created_date")
    private Timestamp createdDate;
}
