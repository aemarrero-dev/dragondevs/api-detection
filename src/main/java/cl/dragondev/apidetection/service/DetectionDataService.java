package cl.dragondev.apidetection.service;

import cl.dragondev.apidetection.entity.DetectionEntity;
import java.util.List;

public interface DetectionDataService {

    List<DetectionEntity> buscarTodos();
}
