package cl.dragondev.apidetection.service.impl;

import cl.dragondev.apidetection.entity.DetectionEntity;
import cl.dragondev.apidetection.repository.DetectionRepository;
import cl.dragondev.apidetection.service.DetectionDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DetectionDataServiceImpl implements DetectionDataService {

    private final DetectionRepository repo;

    @Override
    public List<DetectionEntity> buscarTodos() {
        return repo.findAll();
    }
}
