package cl.dragondev.apidetection.repository;

import cl.dragondev.apidetection.entity.DetectionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DetectionRepository extends JpaRepository<DetectionEntity, Long> {

}
