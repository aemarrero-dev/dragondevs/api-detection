package cl.dragondev.apidetection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiDetectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiDetectionApplication.class, args);
	}

}
