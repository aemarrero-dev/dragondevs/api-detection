package cl.dragondev.apidetection.dto;

import lombok.Data;
import java.sql.Timestamp;

@Data
public class DetectionDTO {

    private Long id;
    private String label;
    private Timestamp detectionDate;
    private String confidenceLevel;
    private float topLeftX;
    private float topLeftY;
    private float bottomRightX;
    private float bottomRightY;
    private String detectionSource;
    private String createAppId;
    private Timestamp createdDate;
}
