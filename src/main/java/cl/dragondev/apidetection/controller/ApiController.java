package cl.dragondev.apidetection.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import cl.dragondev.apidetection.service.DetectionDataService;
import cl.dragondev.apidetection.entity.DetectionEntity;

import java.util.List;

@RestController
@Log4j2
@RequiredArgsConstructor
public class ApiController {

    private final DetectionDataService detectionDataService;

    @GetMapping("/data")
    public ResponseEntity<List<DetectionEntity>> getData() {
        log.info("ApiController: /data");
        return new ResponseEntity<>(detectionDataService.buscarTodos(), HttpStatus.OK);
    }

}
